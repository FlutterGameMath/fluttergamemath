import 'package:flutter/material.dart';
import 'package:mathgame/Multiple.dart';

import '../Plus.dart';
import '../Subteact.dart';
import '../Divide.dart';

class MobileBody extends StatelessWidget {
  const MobileBody({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber[50],
      appBar: AppBar(
        title: Text("เกมส์คณิตคิดเร็ว"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: AspectRatio(
              aspectRatio: 16 / 9,
              child: Container(
                height: 200,
                child: FittedBox(
                  fit: BoxFit.cover,
                  child: Image.asset('assets/math_logo.png', scale: 200),
                ),
              ),
            ),
          ),
          Container(
            child: Text(
              "เลือกด่าน",
              style: TextStyle(fontSize: 24),
            ),
          ),
          Expanded(
            child: ListView(
              padding: const EdgeInsets.all(15.0),
              children: [
                Container(
                  decoration:
                      BoxDecoration(color: Colors.amber[200], boxShadow: [
                    BoxShadow(
                      offset: Offset(0, 10),
                      blurRadius: 50,
                      color: Colors.red.withOpacity(0.53),
                    )
                  ]),
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Plus()));
                    },
                    child: const Text('บวก'),
                    style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      minimumSize: Size(120, 60),
                      textStyle: TextStyle(fontSize: 28),
                      backgroundColor: Colors.amberAccent[200],
                    ),
                  ),
                ),
                SizedBox(height: 15, width: 15),
                Container(
                  decoration:
                      BoxDecoration(color: Colors.amber[200], boxShadow: [
                    BoxShadow(
                      offset: Offset(0, 10),
                      blurRadius: 50,
                      color: Colors.blue.withOpacity(0.53),
                    )
                  ]),
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Sub()));
                    },
                    child: const Text('ลบ'),
                    style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      minimumSize: Size(120, 60),
                      textStyle: TextStyle(fontSize: 28),
                      backgroundColor: Colors.amberAccent[200],
                    ),
                  ),
                ),
                SizedBox(height: 15, width: 15),
                Container(
                  decoration:
                      BoxDecoration(color: Colors.amber[200], boxShadow: [
                    BoxShadow(
                      offset: Offset(0, 10),
                      blurRadius: 50,
                      color: Colors.green.withOpacity(0.53),
                    )
                  ]),
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Multiple()));
                    },
                    child: const Text('คูณ'),
                    style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      minimumSize: Size(120, 60),
                      textStyle: TextStyle(fontSize: 28),
                      backgroundColor: Colors.amberAccent[200],
                    ),
                  ),
                ),
                SizedBox(height: 15, width: 15),
                Container(
                  decoration:
                      BoxDecoration(color: Colors.amber[200], boxShadow: [
                    BoxShadow(
                      offset: Offset(0, 10),
                      blurRadius: 50,
                      color: Colors.purple.withOpacity(0.53),
                    )
                  ]),
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Divide()));
                    },
                    child: const Text('หาร'),
                    style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      minimumSize: Size(120, 60),
                      textStyle: TextStyle(fontSize: 28),
                      backgroundColor: Colors.amberAccent[200],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}

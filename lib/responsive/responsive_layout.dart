import 'package:flutter/material.dart';

class ResponsiveLayout extends StatelessWidget {
  final Widget moblieBody;
  final Widget tabletBody;

  ResponsiveLayout({required this.moblieBody, required this.tabletBody});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      if (constraints.maxWidth < 600) {
        return moblieBody;
      } else {
        return tabletBody;
      }
    });
  }
}

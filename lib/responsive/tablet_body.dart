import 'package:flutter/material.dart';
import 'package:mathgame/Divide.dart';

import '../Plus.dart';
import '../Subteact.dart';
import '../Multiple.dart';

class TabletBody extends StatelessWidget {
  const TabletBody({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber[50],
      appBar: AppBar(
        title: Text("เกมส์คณิตคิดเร็ว"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Expanded(
              child: Column(children: [
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: AspectRatio(
                    aspectRatio: 16 / 9,
                    child: Container(
                      height: 200,
                      color: Colors.amber[50],
                      child: FittedBox(
                        fit: BoxFit.cover,
                        child: Image.asset('assets/math_logo.png', scale: 200),
                      ),
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    "เลือกด่าน",
                    style: TextStyle(fontSize: 36),
                  ),
                ),
                Expanded(
                  child: GridView(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2, childAspectRatio: 16 / 9),
                    padding: const EdgeInsets.all(50.0),
                    children: [
                      Container(
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 10),
                            blurRadius: 50,
                            color: Colors.red.withOpacity(0.53),
                          ),
                        ]),
                        padding: EdgeInsets.all(10.0),
                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Plus()));
                          },
                          child: const Text('บวก'),
                          style: ElevatedButton.styleFrom(
                            elevation: 5,
                            minimumSize: Size(120, 60),
                            textStyle: TextStyle(fontSize: 32),
                            backgroundColor: Colors.amberAccent[200],
                          ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 10),
                            blurRadius: 50,
                            color: Colors.blue.withOpacity(0.53),
                          ),
                        ]),
                        padding: EdgeInsets.all(10.0),
                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) => Sub()));
                          },
                          child: const Text('ลบ'),
                          style: ElevatedButton.styleFrom(
                            elevation: 5,
                            minimumSize: Size(120, 60),
                            textStyle: TextStyle(fontSize: 32),
                            backgroundColor: Colors.amberAccent[200],
                          ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 10),
                            blurRadius: 50,
                            color: Colors.green.withOpacity(0.53),
                          ),
                        ]),
                        padding: EdgeInsets.all(10.0),
                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Multiple()));
                          },
                          child: const Text('คูณ'),
                          style: ElevatedButton.styleFrom(
                            elevation: 5,
                            minimumSize: Size(120, 60),
                            textStyle: TextStyle(fontSize: 32),
                            backgroundColor: Colors.amberAccent[200],
                          ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 10),
                            blurRadius: 50,
                            color: Colors.purple.withOpacity(0.53),
                          ),
                        ]),
                        padding: EdgeInsets.all(10.0),
                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Divide()));
                          },
                          child: const Text('หาร'),
                          style: ElevatedButton.styleFrom(
                            elevation: 5,
                            minimumSize: Size(120, 60),
                            textStyle: TextStyle(fontSize: 32),
                            backgroundColor: Colors.amberAccent[200],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ]),
            ),
          ],
        ),
      ),
    );
  }
}

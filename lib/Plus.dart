import 'dart:async';

import 'package:flutter/material.dart';
import 'dart:math';

import 'package:flutter/services.dart';
import 'package:mathgame/homepage.dart';

class Plus extends StatefulWidget {
  const Plus({Key? key}) : super(key: key);

  @override
  _PlusState createState() => _PlusState();
}

class _PlusState extends State<Plus> {
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (context) => AlertDialog(
                content: const Text('ไปพบกับความสนุกของเกมส์บวกเลขได้เลย !!!'),
                actions: [
                  TextButton(
                      onPressed: () => _startTimer(),
                      child: const Text('เริ่มเกมส์')),
                ],
              ));
    });
  }

  List<String> NumPad = [
    'Cancle',
    'OK',
  ];

  final fieldText = TextEditingController();

  int _count = 1;

  late Timer _timer;

  void _startTimer() {
    _count = 30;
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        if (_count > 0) {
          _count--;
        } else {
          _dialogBuilder(context);
          _timer.cancel();
        }
      });
    });
    Navigator.pop(context);
  }

  var randomNum = Random();

  int number1 = 1;
  int number2 = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          title: const Text(
            'บวกเลข',
            style: TextStyle(color: Colors.black, fontSize: 24),
          ),
          backgroundColor: Colors.amber),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 50,
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'คะแนน: $score',
                style: SetTextStyle,
              ),
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'เวลา: $_count',
                style: SetTextStyle,
              ),
            ),
            (_count > 0)
                ? Text("")
                : Text(
                    "หมดเวลา!",
                    style: SetTextStyle,
                  ),
            //Quiz
            Expanded(
              // ignore: avoid_unnecessary_containers
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('$number1 + $number2 = ', style: SetTextStyle),
                    //input
                    Container(
                      height: 50,
                      width: 100,
                      color: const Color.fromARGB(255, 183, 255, 191),
                      child: Center(
                        
                        child: TextField(
                          textAlign: TextAlign.center, //เพิ่ม
                          style: TextStyle(fontSize: 28,fontWeight: FontWeight.bold),
                          controller: fieldText,
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          autofocus: true,
                          autocorrect: true,
                          onChanged: (text) {
                            ans = text;
                          },
                        ),
                        //Text(input, style: SetTextStyle),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            Expanded(
              flex: 2,
              // ignore: avoid_unnecessary_containers
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: GridView.builder(
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 3,
                      ),
                      itemCount: NumPad.length,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return NumButton(
                          child: NumPad[index],
                          onTap: () => buttonPressFunction(NumPad[index]),
                        );
                      }),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void clearText() {
    fieldText.clear();
  }

  int score = 0;

  var ans = '';

  //buttonPressFunction
  void buttonPressFunction(String button) {
    setState(() {
      if (button == "OK") {
        CheckAns();
        clearText();
      } else if (button == 'Cancle') {
        ans = '';
        clearText();
      }
    });
  }

  void CheckAns() {
    if (ans == '') {
      Continued();
    } else if (number1 + number2 == int.parse(ans)) {
      score++;
      Continued();
    } else {
      Continued();
    }
  }

  void Continued() {
    setState(() {
      ans = '';
    });

    number1 = randomNum.nextInt(10);
    number2 = randomNum.nextInt(10);
  }

  Future<void> _dialogBuilder(BuildContext context) {
    return showDialog<void>(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('หมดเวลา!!!'),
          content: Text('คุณทำคะแนนได้ทั้งหมด $score คะแนน'),
          actions: <Widget>[
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: const Text('ปิด'),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HomePage()));

              },
            ),
          ],
        );
      },
    );
  }
}

class NumButton extends StatelessWidget {
  final String child;
  final VoidCallback onTap;
  var btnColor = const Color.fromARGB(255, 151, 234, 158);

  NumButton({
    Key? key,
    required this.child,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (child == 'Cancle') {
      btnColor = const Color.fromARGB(255, 219, 0, 0);
      
    } else if (child == 'OK') {
      btnColor = const Color.fromARGB(255, 38, 210, 4);
    }
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            color: btnColor,
            borderRadius: BorderRadius.circular(3),
          ),
          child: Center(
            child: Text(
              child,
              style: SetTextStyle,
            ),
          ),
        ),
      ),
    );
  }
}

var SetTextStyle = const TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 32,
    color: Color.fromARGB(255, 0, 0, 0));

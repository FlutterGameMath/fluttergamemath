import 'package:flutter/material.dart';
import 'package:mathgame/responsive/mobile_body.dart';
import 'package:mathgame/responsive/responsive_layout.dart';
import 'package:mathgame/responsive/tablet_body.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ResponsiveLayout(
        moblieBody: MobileBody(),
        tabletBody: TabletBody(),
      ),
    );
  }
}
